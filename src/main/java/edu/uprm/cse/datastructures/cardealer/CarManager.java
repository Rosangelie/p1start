package edu.uprm.cse.datastructures.cardealer;

import java.util.Optional;
import java.util.function.Predicate;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

@Path("/cars")
public class CarManager {
	 private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();  
	 private final HashTableOA<Long, Car> cHashTable = CarTable.getInstance();

	 //This method returns an array with all the cars in the list.
	@GET
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car[] getAllCars() {
	   Car[] result = new Car[cList.size()];
	   for(int i = 0; i < cList.size(); i++) {
		   result[i] = cList.get(i);
	   }
	   return result;
 	  }
	
	/*This method returns the car with the indicated id. 
	 * If it's not in the hashTable, it returns 404 (Not Found).*/
	@GET
	  @Path("{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car getCar(@PathParam("id") long id){

		cHashTable.get(id);
		throw new WebApplicationException(404);
		   
	  }        
	
	/*This method adds the indicated car to the hashTable and 
	 * returns the status code 201 (Created)*/
	 @POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addCar(Car c){
		 
		  cHashTable.put(c.getCarId(), c);
	      return Response.status(201).build();
	    }
	 
	 /*This method updates a car without changing the id 
	  * and returns the status code 200 (OK). If the car is null, it returns 404 (Not Found).*/
	 @PUT
	    @Path("{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCar(Car c){
		 
		 if(c == null) {
			 throw new WebApplicationException(404);
		 }
		 else if(cHashTable.contains(c.getCarId())) {
			 cHashTable.put(c.getCarId(), c);
		 }
		 throw new WebApplicationException(404);
	    }  
	 
	 
	 /*This method deletes the car with the indicated id and returns 
	  * the status code 200 (OK). If it does not remove it, it returns 404 (Not Found).*/
	 @DELETE
	    @Path("{id}/delete")
	    public Response deleteCar(@PathParam("id") long id){

		    cHashTable.remove(id);
			throw new WebApplicationException(404);
	    }    

}

package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	//This class creates the nodes.
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}

		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrevious() {
			return previous;
		}

		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}

	}

	//This class allows us to iterate through all the elements of the list.
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		private CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>)header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}


	private Node<E> header;
	private Comparator <E> comparator;
	private int currentSize;

	//Constructor
	public CircularSortedDoublyLinkedList(Comparator<E> c){
		super();
		this.header = new Node<E>(null,this.header,this.header);
		this.comparator = c;
		this.currentSize = 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	/*This method adds the elements to the Circular Sorted Doubly Linked List 
	   and returns true if the element is added. */
	@Override
	public boolean add(E obj) {
		Node<E> temp = this.header.getNext();
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj, this.header, this.header);
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			this.currentSize++;
			return true;
		}else {
			while(temp != this.header) {
				if(this.comparator.compare(obj, temp.getElement()) < 0) {
					Node<E> newNode = new Node<>(obj,null,null);
					temp.getPrevious().setNext(newNode);
					newNode.setNext(temp);
					newNode.setPrevious(temp.getPrevious());
					temp.setPrevious(newNode);
					this.currentSize++;
					return true;
				}
				temp = temp.getNext();
			}
			Node<E> newNode = new Node<E>(obj,null,null);
			temp.getPrevious().setNext(newNode);
			newNode.setNext(temp);
			newNode.setPrevious(temp.getPrevious());
			temp.setPrevious(newNode);
			this.currentSize++;
			return true;
		}
	}

	//This method returns the size of the Circular Sorted Doubly Linked List.
	@Override
	public int size() {
		return this.currentSize;
	}

	/*This method returns true when the element is removed 
	  and false if the element does not exist. */
	@Override
	public boolean remove(E obj) {
		int temp = this.firstIndex(obj);

		if(temp < 0) {
			return false;
		}
		else {
			this.remove(temp);
			return true;
		}

	}

	/*This method returns true when the element at the indicated index is removed. If the index is less than zero 
	 * or greater than the size of the list, it throws an Index Out Of Bounds Exception. */
	@Override
	public boolean remove(int index) {

		if((index < 0) || (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = this.getNode(index);
		
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		temp.setElement(null);
		temp.setNext(null);
		temp.setPrevious(null);
		this.currentSize--;
		return true;

	}

	/*This method removes all nodes with the indicated element 
	  and returns the amount of elements removed.*/
	@Override
	public int removeAll(E obj) {		
		int count = 0;
		while(this.remove(obj)) {
			count++;
		}
		return count;
	}

	//This method returns the first element in the Circular Sorted Doubly Linked List
	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	//This method returns the last element in the Circular Sorted Doubly Linked List
	@Override
	public E last() {
		return this.header.getPrevious().getElement();
	}

	//This method returns the element at the indicated position.
	@Override
	public E get(int index) {	
		return this.getNode(index).getElement();
	}

	//This method removes all elements from the list.
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}

	/*This method returns true if the list contains the indicated element 
	 * and false if the element is not in the list.*/
	@Override
	public boolean contains(E e) {
		Node<E> temp = this.header.getNext();

		while(temp != this.header) {
			if(temp.getElement().equals(e)) {
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	//This method returns true if the list is empty and false if the size is not zero (not empty).
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/*This method returns the first index of the indicated element. If the element 
	  is not in the list returns -1. */
	@Override
	public int firstIndex(E e) {
		int currentPosition = 0;
		Node<E> temp = this.header.getNext();

		while(temp != this.header) {
			if(temp.getElement().equals(e)) {
				return currentPosition;
			}
			temp = temp.getNext();
			currentPosition++;
		}
		return -1;
	}

	/*This method returns the last index of the indicated element. If the element is
	  not in the list returns -1. */
	@Override
	public int lastIndex(E e) {
		int i=this.size()-1;
		for(Node<E> temp = this.header.getPrevious(); temp != this.header; temp = temp.getPrevious(), --i) {
			if(temp.getElement().equals(e)) {
				return i;
			}
		}
		return -1;

	}
	
	/*This method returns the node with the indicated index. If the index is less than zero 
	 * or greater than the size of the list, it throws an Index Out Of Bounds Exception.*/
	private Node<E> getNode(int index){

		if((index < 0) || (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}

		int currentPosition = 0;
		Node<E> temp = this.header.getNext();
		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;
	}
}

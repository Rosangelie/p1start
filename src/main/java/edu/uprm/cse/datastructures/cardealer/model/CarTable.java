package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
	
	private static HashTableOA<Long, Car> cHashTable = new HashTableOA<>();

	private CarTable(){}

	public static HashTableOA<Long, Car> getInstance(){
		return cHashTable;
	}

	public static void resetCars() {
		cHashTable = new HashTableOA<>();
	}
}


package edu.uprm.cse.datastructures.cardealer.util;

public class HashTableOA<K, V> implements Map<K, V> {
	
	private enum BucketStatus {
		FULL,
		EMPTY,
		NEW
	}
	
	private static class MapEntry<K, V>{
		private K key;
		private V value;
		private BucketStatus state;
		
		public MapEntry(K key, V value, BucketStatus state) {
			super();
			this.key = key;
			this.value = value;
			this.state = state;
		}
		
		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public BucketStatus getState() {
			return state;
		}
		public void setState(BucketStatus state) {
			this.state = state;
		}
		
	}
	
	private int currentSize;
	private Object[] buckets;
	private static final int DEFAULT_SIZE = 10;
	
	public HashTableOA(int numBuckets) {
		this.currentSize = 0;
		this.buckets = new Object[numBuckets];
		
		for(int i=0; i < numBuckets; ++i) {
			this.buckets[i] = new MapEntry<K,V>(null, null, BucketStatus.NEW);
		}
	}
	
	public HashTableOA() {
		this(DEFAULT_SIZE);
	}
	
	//HashFunction Method 1
	private int hashFunction(K key) {
		return key.hashCode() % this.buckets.length;
	}
	
	//Hashfunction Method 2
	private int hashFunction2(K key) {
		return (int) Math.pow(key.hashCode(), 2) % this.buckets.length;  //CHECK
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		
		int targetBucket = this.hashFunction(key);
		MapEntry<K, V> B = (MapEntry<K,V>)this.buckets[targetBucket];
		
		if(B.getState() == BucketStatus.NEW) {
			return null;
		}
		else if(B.getState() == BucketStatus.FULL && B.getKey().equals(key)) {
			return B.getValue();
		}
		else {
			//2nd hash function method
			int targetBucket2 = this.hashFunction2(key);
			
			while(targetBucket2 != targetBucket) {
				B = (MapEntry<K, V>)this.buckets[targetBucket2];


				if(B.getState() == BucketStatus.NEW) {
					return null;
				}
				else if(B.getState() == BucketStatus.FULL && B.getKey().equals(key)) {
					return B.getValue();
				}
				else {
					targetBucket2 = (targetBucket2+1) % this.buckets.length; //Probing to avoid collision - CHECK
				}
			}
			return null;
		}
	}

	@Override
	public V put(K key, V value) {
		this.remove(key); //avoids duplicates
		
		if(this.currentSize == this.buckets.length) {
			this.reAllocate();
		}
		
		int targetBucket = this.hashFunction(key);
		MapEntry<K, V> B = (MapEntry<K,V>)this.buckets[targetBucket];
		
		if(B.getState().equals(BucketStatus.NEW)) {
			B.setKey(key);
			B.setValue(value);
			B.setState(BucketStatus.FULL);
			++this.currentSize;
			return B.getValue();
			
		}
		else {
			//Collision
			int targetBucket2 = this.hashFunction2(key);

			while(targetBucket2 != targetBucket) {
				B = (MapEntry<K, V>)this.buckets[targetBucket2];


				if(B.getState() == BucketStatus.NEW) {
					B.setKey(key);
					B.setValue(value);
					B.setState(BucketStatus.FULL);
					++this.currentSize;
					
				}
				else {
					targetBucket2 = (targetBucket2+1) % this.buckets.length; //Probing to avoid collision - CHECK
				}
			}
			return B.getValue();
		}
	}

	private void reAllocate() {
		//To implement
		
	}

	@Override
	public V remove(K key) {

		if(key == null) {
			throw new IllegalArgumentException("Key cannot be null");
		}
		
		int targetBucket = this.hashFunction(key);
		MapEntry<K, V> B = (MapEntry<K,V>)this.buckets[targetBucket];

		if(B.getState() == BucketStatus.NEW) {
			return null;
		}
		else if(B.getState() == BucketStatus.FULL && B.getKey().equals(key)) {
			V result = B.getValue();
			B.setKey(null);
			B.setValue(null);
			B.setState(BucketStatus.EMPTY);
			return result;
		}
		else {
			int targetBucket2 = this.hashFunction2(key);

			while(targetBucket2 != targetBucket) {
				B = (MapEntry<K, V>)this.buckets[targetBucket2];


				if(B.getState() == BucketStatus.NEW) {
					return null;
				}
				else if(B.getState() == BucketStatus.FULL && B.getKey().equals(key)) {
					V result = B.getValue();
					B.setKey(null);
					B.setValue(null);
					B.setState(BucketStatus.EMPTY);
					return result;
				}
				else {
					targetBucket2 = (targetBucket2+1) % this.buckets.length; //Probing to avoid collision - CHECK
				}
			}
			return null;
		}
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}

	@Override
	public SortedList<K> getKeys() {
		SortedList<K> keys = new CircularSortedDoublyLinkedList<K>(null); //CHECK
		MapEntry<K, V> B = null;
		
		for(int i=0; i< this.buckets.length; ++i) {
			B = (MapEntry<K, V>)this.buckets[i];
			if(B.getState().equals(BucketStatus.FULL)) {
				keys.add(B.getKey());
			}
		}
		return keys;
	}

	@Override
	public SortedList<V> getValues() {
		SortedList<V> values = new CircularSortedDoublyLinkedList<V>(null); //CHECK
		MapEntry<K, V> B = null;
		
		for(int i=0; i< this.buckets.length; ++i) {
			B = (MapEntry<K, V>)this.buckets[i];
			if(B.getState().equals(BucketStatus.FULL)) {
				values.add(B.getValue());
			}
		}
		return values;
	}

}
